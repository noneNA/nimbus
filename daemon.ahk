#SingleInstance Prompt
;#Warn

; https://autohotkey.com/board/topic/40638-controlclick-dragging/
IsWindowChildOf(aChild, aParent)
{
    static EnumChildFindHwndProc=0
    if !EnumChildFindHwndProc
        EnumChildFindHwndProc := RegisterCallback("EnumChildFindHwnd","Fast")
    VarSetCapacity(lParam,8,0), NumPut(aChild,lParam,0)
    DllCall("EnumChildWindows","uint",aParent,"uint",EnumChildFindHwndProc,"uint",&lParam)
    return NumGet(lParam,4)
}

; https://autohotkey.com/board/topic/40638-controlclick-dragging/
; Accepts coords relative to the specified window.
;   X and Y MUST be specified, since current actual mouse location is irrelevant.
;   Control:
;       This can be, ClassNN, the name/text of the control, or a control HWND
;       (which must be a child of the target window.)
;       If a control HWND is specified, X and Y will be relative to the target
;       window, not the child control.
;   Options:
;       a string of letters (spaces are optional) indicating which buttons("LMR X1 X2")/keys("S C"=shift,control) should be considered pressed.
;       K: use actual key state for Shift and Ctrl.
;   Return value:
;       The hwnd of the control which was sent the mousemove message.
;       Pass this to the Control parameter to simulate mouse capture.
; Based on AutoHotkey::script2.cpp::ControlClick()
ControlMouseMove(X, Y, Control="", WinTitle="", WinText="", Options="", ExcludeTitle="", ExcludeText="")
{
    static EnumChildFindPointProc=0
    if !EnumChildFindPointProc
        EnumChildFindPointProc := RegisterCallback("EnumChildFindPoint","Fast")
    
    if !(target_window := WinExist(WinTitle, WinText, ExcludeTitle, ExcludeText))
        return false
    if Control
    {
        if Control is integer
            Control_is_hwnd := IsWindowChildOf(Control, target_window)
        
        if Control_is_hwnd
        {
            ; If %Control% specifies a control hwnd, send it the mouse move,
            ; but use coords relative to the window specified by WinTitle.
            ; This can be used to more easily simulate mouse capture.
            control_window := Control
            VarSetCapacity(rect, 16)
            DllCall("GetWindowRect","uint",target_window,"uint",&rect)
            VarSetCapacity(child_rect, 16)
            DllCall("GetWindowRect","uint",control_window,"uint",&child_rect)
            X -= NumGet(child_rect,0,"int") - NumGet(rect,0,"int")
            Y -= NumGet(child_rect,4,"int") - NumGet(rect,4,"int")
        }
        else
            ControlGet, control_window, Hwnd,, %Control%, ahk_id %target_window%
    }
    if (!control_window)
    {
        VarSetCapacity(rect, 16)
        DllCall("GetWindowRect","uint",target_window,"uint",&rect)
        VarSetCapacity(pah, 36, 0)
        NumPut(X + NumGet(rect,0,"int"), pah,0,"int")
        NumPut(Y + NumGet(rect,4,"int"), pah,4,"int")
        DllCall("EnumChildWindows","uint",target_window,"uint",EnumChildFindPointProc,"uint",&pah)
        control_window := NumGet(pah,24) ? NumGet(pah,24) : target_window
        DllCall("ScreenToClient","uint",control_window,"uint",&pah)
        X:=NumGet(pah,0,"int"), Y:=NumGet(pah,4,"int")
    }
    wParam :=  (InStr(Options,"L") ? 0x1 : 0) || (InStr(Options,"M") ? 0x10 : 0) || (InStr(Options,"R") ? 0x2 : 0)
            || (InStr(Options,"X1") ? 0x20 : 0) || (InStr(Options,"X2") ? 0x40 : 0)
            || (InStr(Options,"S") ? 0x4 : 0) || (InStr(Options,"C") ? 0x8 : 0)
            || (InStr(Options,"K") ? (GetKeyState("Shift") ? 0x4:0)|(GetKeyState("Control") ? 0x8:0) : 0)
    PostMessage, 0x200, wParam, (x & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
    return control_window
}

; Modified version that doesn't change given position and clicks at the given spot
ControlMouseMoveClick(X, Y, Control="", WinTitle="", WinText="", Options="", ExcludeTitle="", ExcludeText="", Delay=0)
{
    static EnumChildFindPointProc=0
    if !EnumChildFindPointProc
        EnumChildFindPointProc := RegisterCallback("EnumChildFindPoint","Fast")
    
    if !(target_window := WinExist(WinTitle, WinText, ExcludeTitle, ExcludeText))
        return false
    if Control
    {
        if Control is integer
            Control_is_hwnd := IsWindowChildOf(Control, target_window)
        
        if Control_is_hwnd
        {
            ; If %Control% specifies a control hwnd, send it the mouse move,
            ; but use coords relative to the window specified by WinTitle.
            ; This can be used to more easily simulate mouse capture.
            control_window := Control
            VarSetCapacity(rect, 16)
            DllCall("GetWindowRect","uint",target_window,"uint",&rect)
            VarSetCapacity(child_rect, 16)
            DllCall("GetWindowRect","uint",control_window,"uint",&child_rect)
            X -= NumGet(child_rect,0,"int") - NumGet(rect,0,"int")
            Y -= NumGet(child_rect,4,"int") - NumGet(rect,4,"int")
        }
        else
            ControlGet, control_window, Hwnd,, %Control%, ahk_id %target_window%
    }
    if (!control_window)
    {
        VarSetCapacity(rect, 16)
        DllCall("GetWindowRect","uint",target_window,"uint",&rect)
        VarSetCapacity(pah, 36, 0)
        NumPut(X + NumGet(rect,0,"int"), pah,0,"int")
        NumPut(Y + NumGet(rect,4,"int"), pah,4,"int")
        DllCall("EnumChildWindows","uint",target_window,"uint",EnumChildFindPointProc,"uint",&pah)
        control_window := NumGet(pah,24) ? NumGet(pah,24) : target_window
        DllCall("ScreenToClient","uint",control_window,"uint",&pah)
        ; X:=NumGet(pah,0,"int"), Y:=NumGet(pah,4,"int")
    }
    wParam :=  (InStr(Options,"L") ? 0x1 : 0) || (InStr(Options,"M") ? 0x10 : 0) || (InStr(Options,"R") ? 0x2 : 0)
            || (InStr(Options,"X1") ? 0x20 : 0) || (InStr(Options,"X2") ? 0x40 : 0)
            || (InStr(Options,"S") ? 0x4 : 0) || (InStr(Options,"C") ? 0x8 : 0)
            || (InStr(Options,"K") ? (GetKeyState("Shift") ? 0x4:0)|(GetKeyState("Control") ? 0x8:0) : 0)

	
	VarSetCapacity(pos, 8)
	DllCall("GetCursorPos", "Ptr", &pos)
	_x := NumGet(pos, 0, "Int")
	_y := NumGet(pos, 4, "Int")
	
	;DllCall("SetCursorPos", "int", _x + 2, "int", _y + 2)
	;SendMessage, 0x200,, ((_x + 2) & 0xFFFF) | (((_y + 2) & 0xFFFF)<<16),, ahk_id %control_window%
	;Sleep, 17
	;DllCall("SetCursorPos", "int", _x, "int", _y)
	;SendMessage, 0x200,, ((_x + 0) & 0xFFFF) | (((_y + 0) & 0xFFFF)<<16),, ahk_id %control_window%
	
	SendMode Event
	;SendMode Input
	CoordMode, Mouse, Screen
	MouseMove, _x + 2, _y + 2, 0
	MouseMove, _x, _y, 0
	
	if (Delay != 0)
	{
		Sleep, %Delay%
	}
	
	;ControlClick, x%x% y%y%, ahk_id %control_window%

	;if (Delay != 0)
	;{
	;	Loop, %Delay%
	;	{
	;		SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;		PostMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;		Sleep, 1
	;	}
	;}
	;
	;SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x + 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x - 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	
	SendMessage, 0x201, 0x0001, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	
	;SendMessage, 0x200, wParam, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200, wParam, ((x + 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	SendMessage, 0x200, wParam, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200, wParam, ((x - 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200, wParam, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	
	SendMessage, 0x202, 0x0000, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	
	SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x + 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x - 2) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;
	;if (Delay != 0)
	;{
	;	Loop, %Delay%
	;	{
	;		SendMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;		PostMessage, 0x200,, ((x + 0) & 0xFFFF) | ((y & 0xFFFF)<<16),, ahk_id %control_window%
	;		Sleep, 1
	;	}
	;}

	
	return control_window
}

get_window()
{
	WinGet, headless_id, ID, Risk of Rain [ HEADLESS ]
	return %headless_id%
}

get_active()
{
	WinGet, active_id, ID, A
	return %active_id%
}

get_mouse()
{

}

click(window, x, y)
{
	Sleep, 100
	ControlMouseMoveClick(x, y,, "ahk_id " . window, "", "L K", "", "", 16 + 1)
	Sleep, 100
}

key(window, key)
{
	Sleep, 100
	ControlSend,, %key%, ahk_id %window%
	Sleep, 100
}

online()
{
	window := get_window()
	if window
	{
		click(window, 400, 180)
	}
}

host()
{
	window := get_window()
	if window
	{
		click(window, 400, 140)
	}
}

select()
{
	window := get_window()
	if window
	{
		click(window, 320, 300)
		;Sleep, 20
		;click(window, 320, 300)
	}
}

launch()
{
	window := get_window()
	if window
	{
		click(window, 400, 400)
	}
}

ready()
{
	window := get_window()
	if window
	{
		click(window, 400, 400)
	}
}

mainmenu()
{
	window := get_window()
	if window
	{
		key(window, "{Escape}")
		click(window, 400, 360)
		click(window, 400, 290)
	}
}

skip()
{

}

headless_command(command)
{
	if (command = "online")
	{
		online()
	}
	else if (command == "host")
	{
		host()
	}
	else if (command == "launch")
	{
		launch()
	}
	else if (command == "ready")
	{
		ready()
	}
	else if (command == "select")
	{
		select()
	}
	else if (command == "return")
	{
		mainmenu()
	}
}

; TODO
COMMAND_PREFIX := "\(\) HEADLESS: "

FormatTime, date_var, A_Now, d-M-yyyy
logname := Format("../../logs/log {1}.txt", date_var)

log := FileOpen(logname, "r")
if not log
{
	MsgBox, Couldn't open log: %logname%
	Exit
}

log.Seek(0, 2)
while true
{
	last := log.Tell()
	line := log.ReadLine()
	if not line
	{
		Sleep, 200
		log.Seek(last)
	}
	else
	{
		window := get_window()
		if not window
		{
			; Window died
			while true
			{
				if not get_window()
				{
					Sleep, 200
				}
				else
				{
					; Window back
					log.Seek(0, 2)
					break
				}
			}
		}
		else if (not WinActive("ahk_id " . window))
		{
			pos := RegExMatch(line, "O)^" . COMMAND_PREFIX . "(.*)$", match)
			if (pos != 0)
			{
				headless_command(match.Value(1))
			}
		}
	}
}