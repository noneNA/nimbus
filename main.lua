-- Nimbus - main.lua

-- Dependencies:
---- Nothing

require("general")

if modloader.getProfileName() == "HEADLESS" then
	require("headless")
end