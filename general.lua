-- Nimbus - general.lua

-- Dependencies:
---- Nothing

local host_player

callback.register("onGameStart", function()
	host_player = nil
end)

local host_setter = net.Packet.new("host_setter", function(sender, net_player)
	local player = net_player:resolve()
	if player and player:isValid() then
		assert(sender == player)
		host_player = player
	end
end)

callback.register("onStep", function()
	if host_player then
		host_player.x = 0
		host_player.y = 0
		--host_player:set("invincible", 2^10)
		if host_player:get("dead") == 0 then
			host_player:kill()
		end
	end
	if net.online and net.host and host_player == nil then
		local player = net.localPlayer or misc.players[1]
		host_player = player
		host_setter:sendAsHost(net.ALL, nil, player:getNetIdentity())
	end
end)