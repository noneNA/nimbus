# Nimbus

This is a mod + daemon for running a psuedo-headless server for [Risk of Rain Modloader](https://rainfusion.ml).  

## Usage

### Host

#### Linux

* Install xdotool
* Make a profile named `HEADLESS`
* Enable this mod and the other mods you want
* Launch the game (Check that your profile is HEADLESS in the main menu)
* Switch off fullscreen and set the resolution to 800x600
* Go to the main menu
* Run `daemon.py` and wait for the launch button to appear
* The daemon will re-host when it should

#### Windows

* Install AutoHotkey
* Make a profile named `HEADLESS`
* Enable this mod and the other mods you want
* Launch the game (Check that your profile is HEADLESS in the main menu)
* Switch off fullscreen and set the resolution to 800x600
* Go to the main menu
* Run `daemon.ahk` and wait for the launch button to appear
* The daemon will re-host when it should

### Client

* Join the server
* Hit READY only when everyone joins (The game is started when everyone hits READY)