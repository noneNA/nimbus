#!/usr/bin/env python3

import sys
import datetime
import time
import os
import subprocess
import re

COMMAND_PREFIX = "() HEADLESS: "
UNIT_TIME = 1/4
LOG_SLEEP = 1/4

def get_window():
	return subprocess.run(["xdotool", "search", "--name", "Risk of Rain \\[ HEADLESS \\]"], stdout = subprocess.PIPE).stdout.strip().decode("utf-8")

def get_active():
	return subprocess.run(["xdotool", "getactivewindow"], stdout = subprocess.PIPE).stdout.strip().decode("utf-8")

def get_mouse():
	out = subprocess.run(["xdotool", "getmouselocation", "--shell"], stdout = subprocess.PIPE).stdout.strip().decode("utf-8")
	x_match = re.search("X=(.*)", out)
	y_match = re.search("Y=(.*)", out)
	return x_match.group(1), y_match.group(1)

def click(window, x, y):
	_window = get_active()
	_x, _y = get_mouse()
	subprocess.run(["xdotool", "windowactivate", "--sync", window])
	subprocess.run(["xdotool", "mousemove", "--window", window, str(x), str(y)])
	time.sleep(UNIT_TIME)
	subprocess.run(["xdotool", "mousedown", "1"])
	time.sleep(UNIT_TIME)
	subprocess.run(["xdotool", "mouseup", "1"])
	#time.sleep(UNIT_TIME)
	subprocess.run(["xdotool", "mousemove", _x, _y])
	subprocess.run(["xdotool", "windowactivate", "--sync", _window])

def key(window, keys):
	_window = get_active()
	subprocess.run(["xdotool", "windowactivate", "--sync", window])
	for key in keys:
		subprocess.run(["xdotool", "keydown", "--window", window, key])
		time.sleep(UNIT_TIME)
		subprocess.run(["xdotool", "keyup", "--window", window, key])
		time.sleep(UNIT_TIME)
	subprocess.run(["xdotool", "windowactivate", "--sync", _window])

def online():
	window = get_window()
	if window != "":
		click(window, 400, 180)

def host():
	window = get_window()
	if window != "":
		click(window, 400, 140)

def select():
	window = get_window()
	if window != "":
		click(window, 320, 300)
		click(window, 320, 300)

def launch():
	window = get_window()
	if window != "":
		click(window, 400, 400)

def ready():
	window = get_window()
	if window != "":
		click(window, 400, 400)

def mainmenu():
	window = get_window()
	if window != "":
		key(window, ["Escape"])
		time.sleep(1/2)
		click(window, 400, 360)
		click(window, 400, 290)

def skip():
	pass
    
def headless_command(command):
	if command == "online":
		online()
	elif command == "host":
		host()
	elif command == "launch":
		launch()
	elif command == "ready":
		ready()
	elif command == "select":
		select()
	elif command == "return":
		mainmenu()

log = False
logname = datetime.datetime.now().strftime("log %-d-%-m-%-Y.txt")

filename = os.path.join("../../logs", logname)
try:
	log = open(filename)
except:
	print(f"Couldn't open: {filename} {os.path.abspath(filename)}")
	exit()

log.seek(0, 2)
while True:
	try:
		last = log.tell()
		line = log.readline()
		if not line:
			time.sleep(LOG_SLEEP)
			log.seek(last)
		else:
			match = re.match(f"^{re.escape(COMMAND_PREFIX)}(.*)", line.strip())
			if match:
				if get_window() == "":
					print("Window died")
					while True:
						if get_window() == "":
							time.sleep(1/2)
						else:
							print("Window back")
							log.seek(0, 2)
							break
				else:
					command = match.group(1)
					print(f"Executing {command}")
					headless_command(command)
	except KeyboardInterrupt:
		break