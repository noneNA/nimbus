-- Nimbus - headless.lua

-- Dependencies:
---- Nothing

local COMMAND_PREFIX = "() HEADLESS: "
local TIMEOUT = 3 * 60

local o_preplayer = Object.find("PrePlayer")

local r_start = Room.find("Start")
local r_host = Room.find("Host")
local r_selectmult = Room.find("SelectMult")
local RETURN = {
	[Room.find("Storage")] = true,
	[Room.find("2CutScene2")] = true,
	[Room.find("2CutScene3")] = true,
	[Room.find("Credits")] = true,
}

local bus = {}
local function headless_run(command)
	log(COMMAND_PREFIX .. tostring(command))
end
local function headless_probe(command)
	if bus[command] then
		return
	else
		headless_run(command)
		bus[command] = TIMEOUT
	end
end
callback.register("globalStep", function()
	for command,timer in pairs(bus) do
		if timer <= 0 then
			bus[command] = nil
		else
			bus[command] = bus[command] - 1
		end
	end
end)

local function allReady()
	if not Room.getCurrentRoom() == selectmult then return false end
	if #o_preplayer:findAll() == 1 then return false end
	for _,i_preplayer in ipairs(o_preplayer:findAll()) do
		if i_preplayer:get("ready") == 0 then
			return false
		end
	end
	return true
end

local function localPrePlayer()
	for _,i_preplayer in ipairs(o_preplayer:findAll()) do
		if i_preplayer:get("ping") == "" then
			return i_preplayer
		end
	end
end

local function notReady()
	local i_preplayer = localPrePlayer()
	return (i_preplayer ~= nil) and i_preplayer:get("ready") == 0
end

local function notSelected()
	local i_preplayer = localPrePlayer()
	return (i_preplayer ~= nil) and i_preplayer:get("class") == -1
end

local function allDead()
	for _,player in ipairs(misc.players) do
		if player:get("dead") ~= 1 then
			return false
		end
	end
	return true
end

local function alone()
	return #misc.players == 1
end

callback.register("onStep", function()
	if allDead() or alone() then
		headless_probe("return")
	end
end)

callback.register("globalStep", function()
	local current_room = Room.getCurrentRoom()
	if current_room == r_start then
		headless_probe("online")
	elseif current_room == r_host then
		headless_probe("host")
	elseif current_room == r_selectmult then
		if notSelected() then
			headless_probe("select")
		elseif notReady() then
			headless_probe("ready")
		elseif allReady() then
			headless_probe("launch")
		end
	elseif RETURN[current_room] then
		headless_probe("return")
	end
end)